library(ggplot2)
library(dplyr)

injuryDat <- read.csv('injury_data_by_sex_eth_2001-2014.csv', stringsAsFactors=F) %>% 
	mutate(Age.in.Years = as.numeric(Age.in.Years),
	       Population = as.numeric(gsub(",", "", Population)),
	       Crude.Rate = Injuries/Population * 100000,
	       Crude.Rate.Lower = Lower.95..Confidence.Limit/Population * 100000,
	       Crude.Rate.Upper = Upper.95..Confidence.Limit/Population * 100000)

minChildhood <- min(injuryDat$Crude.Rate[injuryDat$Age.in.Years %in% 2:10 & injuryDat$Sex=="B" & injuryDat$Race.Ethnicity=="A"])
maxAdol <- max(injuryDat$Crude.Rate[injuryDat$Age.in.Years %in% 10:30 & injuryDat$Sex=="B" & injuryDat$Race.Ethnicity=="A"])

aPlot <- injuryDat %>%
  filter(Sex=="B", Race.Ethnicity=="A") %>%
  ggplot(aes(x=Age.in.Years, y=Crude.Rate))+
	geom_rect(xmin=10, xmax=22, ymin=-1000, ymax=20000, fill='#ffeeee')+
	geom_hline(yintercept=c(minChildhood, maxAdol), linetype=2, alpha=.2)+
	geom_point()+
	geom_errorbar(aes(ymin=Crude.Rate.Lower, ymax=Crude.Rate.Upper), width=0)+
	coord_cartesian(ylim=c(0,19000), xlim=c(0,86))+
	scale_x_continuous(breaks=5*0:16)+
	theme(panel.background=element_rect(fill='white'))+
	labs(x='Age in years', y='Crude rate of unintentional injury\nwith 95% confidence intervals', title='Unintentional injury over the lifespan\n(CDC data 2001-2014)')
ggsave('unintentional_injury_data.png', aPlot, dpi=150, width=10, height=8)

minChildhood.f <- min(injuryDat$Crude.Rate[injuryDat$Age.in.Years %in% 2:10 & injuryDat$Sex %in% c("F") & injuryDat$Race.Ethnicity=="A"])
maxAdol.f <- max(injuryDat$Crude.Rate[injuryDat$Age.in.Years %in% 10:30 & injuryDat$Sex %in% c("F") & injuryDat$Race.Ethnicity=="A"])
minChildhood.m <- min(injuryDat$Crude.Rate[injuryDat$Age.in.Years %in% 2:10 & injuryDat$Sex %in% c("M") & injuryDat$Race.Ethnicity=="A"])
maxAdol.m <- max(injuryDat$Crude.Rate[injuryDat$Age.in.Years %in% 10:30 & injuryDat$Sex %in% c("M") & injuryDat$Race.Ethnicity=="A"])

aPlot_sex <- injuryDat %>%
  filter(Sex %in% c("M", "F"), Race.Ethnicity=="A") %>%
  ggplot(aes(x=Age.in.Years, y=Crude.Rate))+
  geom_rect(xmin=10, xmax=22, ymin=-1000, ymax=20000, fill='#ffeeee')+
  geom_hline(data=data.frame(minmax=c(minChildhood.f, maxAdol.f, minChildhood.m, maxAdol.m),
		 Sex=c("F", "F", "M", "M")),
	     aes(yintercept=minmax, color=Sex), 
	     linetype=2, alpha=1)+
  geom_point(aes(group=Sex, color=Sex))+
  geom_errorbar(aes(ymin=Crude.Rate.Lower, ymax=Crude.Rate.Upper, group=Sex, color=Sex), width=0)+
  scale_color_manual(breaks=c("F", "M"), labels=c("Female", "Male"), values=c('#998ec3','#f1a340'))+
  coord_cartesian(ylim=c(0,19000), xlim=c(0,86))+
  scale_x_continuous(breaks=5*0:16)+
  theme(panel.background=element_rect(fill='white'))+
  labs(x='Age in years', y='Crude rate of unintentional injury\nwith 95% confidence intervals', title='Unintentional injury over the lifespan\n(CDC data 2001-2014)')
ggsave('unintentional_injury_data_by_sex.png', aPlot_sex, dpi=150, width=10, height=8)

aPlot_eth <- injuryDat %>%
  filter(Sex=="B", !Race.Ethnicity %in% c("A", "N")) %>%
  ggplot(aes(x=Age.in.Years, y=Crude.Rate))+
  geom_rect(xmin=10, xmax=22, ymin=-1000, ymax=20000, fill='#ffeeee')+
  geom_ribbon(aes(ymin=Crude.Rate.Lower, ymax=Crude.Rate.Upper, group=Race.Ethnicity, fill=Race.Ethnicity), position=position_dodge(w=1), alpha=.2)+
  geom_line(aes(group=Race.Ethnicity, color=Race.Ethnicity), position=position_dodge(w=1))+
  geom_point(aes(group=Race.Ethnicity, color=Race.Ethnicity), position=position_dodge(w=1))+
  coord_cartesian(ylim=c(0,19000), xlim=c(0,86))+
  scale_color_manual(breaks=c("W", "B", "H", "O"), labels=c("White", "Black", "Hispanic", "Other (Non-Hisp.)"), values=c('#1b9e77','#d95f02','#7570b3','#e7298a'))+
  scale_fill_manual(breaks=c("W", "B", "H", "O"), labels=c("White", "Black", "Hispanic", "Other (Non-Hisp.)"), values=c('#1b9e77','#d95f02','#7570b3','#e7298a'))+
  scale_x_continuous(breaks=5*0:16)+
  theme(panel.background=element_rect(fill='white'))+
  labs(x='Age in years', y='Crude rate of unintentional injury\nwith 95% confidence intervals', color= "Race/Ethnicity", fill= "Race/Ethnicity", title='Unintentional injury over the lifespan\n(CDC data 2001-2014)')
ggsave('unintentional_injury_data_by_eth.png', aPlot_eth, dpi=150, width=10, height=8)
