Data downloaded from [CDC.gov](http://webappa.cdc.gov/sasweb/ncipc/nfirates2001.html) on 8 Aug 2016. 

>Centers for Disease Control and Prevention. Web-based Injury Statistics Query and Reporting System (WISQARS) [Online]. (2003). National Center for Injury Prevention and Control, Centers for Disease Control and Prevention (producer). Available from: URL: www.cdc.gov/ncipc/wisqars.

From the [CDC website](http://www.cdc.gov/ncipc/wisqars/nonfatal/definitions.htm), section 4.2.3: "Please note that annualized national estimates presented for each race/ethnicity category will be low because they do not count cases recorded as "unknown" race/ethnicity."

![](https://gitlab.com/jflournoy/Adolescent_Injury_Risk/raw/master/unintentional_injury_data.png)
![](https://gitlab.com/jflournoy/Adolescent_Injury_Risk/raw/master/unintentional_injury_data_by_sex.png)
![](https://gitlab.com/jflournoy/Adolescent_Injury_Risk/raw/master/unintentional_injury_data_by_eth.png)
